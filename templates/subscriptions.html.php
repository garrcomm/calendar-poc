<?php
/* @var $subscriptions \Microsoft\Graph\Model\Subscription[] */
/* @var $homeUrl string */
/* @var $subscribeUrl string */
/* @var $unsubscribeUrl string */
/* @var $refreshUrl string */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Subscriptions</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm">
            <p>
                <a href="<?= htmlspecialchars($homeUrl) ?>" class="btn btn-primary">🏠 Home</a>
            </p>
            <h1>Active subscriptions</h1>
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Resource</th>
                        <th>changeType</th>
                        <th>Notification URL</th>
                        <th>Expires at</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($subscriptions as $subscription): ?>
                        <tr>
                            <td><?= htmlspecialchars($subscription->getId()) ?></td>
                            <td><?= htmlspecialchars($subscription->getResource()) ?></td>
                            <td><?= htmlspecialchars($subscription->getChangeType()) ?></td>
                            <td><?= htmlspecialchars($subscription->getNotificationUrl()) ?></td>
                            <td><?= $subscription->getExpirationDateTime()->format('Y-m-d H:i:s') ?></td>
                            <td style="white-space: nowrap">
                                <a class="btn btn-outline-primary" href="<?= htmlspecialchars(str_replace('$id', $subscription->getId(), $unsubscribeUrl)) ?>">🗑️</a>
                                <a class="btn btn-outline-primary" href="<?= htmlspecialchars(str_replace('$id', $subscription->getId(), $refreshUrl)) ?>">🔄</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <a href="<?= htmlspecialchars($subscribeUrl) ?>" class="btn btn-outline-primary">➕ Subscribe to calendar</a>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>
