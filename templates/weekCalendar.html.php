<?php
/* @var $events Microsoft\Graph\Model\Event[] */
/* @var $homeUrl string */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Week calendar</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm">
            <p>
                <a href="<?= htmlspecialchars($homeUrl) ?>" class="btn btn-primary">🏠 Home</a>
            </p>
            <h1>Calendar items in the current week</h1>
            <table class="table">
                <thead>
                    <tr>
                        <th>Subject</th>
                        <th>Starts at</th>
                        <th>Ends at</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($events as $event): ?>
                        <tr>
                            <td><?= htmlspecialchars($event->getSubject()) ?></td>
                            <td><?= $event->getStart()->getDateTime() ?></td>
                            <td><?= $event->getEnd()->getDateTime() ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>
