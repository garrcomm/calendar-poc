<?php
/* @var $baseHref string */
/* @var $logbook string */
/* @var $accessToken League\OAuth2\Client\Token\AccessToken|null */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Microsoft Calendar POC</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm">
            <h1>Microsoft Calendar POC</h1>
            <p>
                With this proof of concept it's possible to authenticate against Microsoft, view the calendar and subscribe to the calendar.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="d-grid gap-2">
                <a href="<?= htmlspecialchars($baseHref) ?>auth/login" class="btn btn-primary">🔑 Login at Microsoft</a>
                <a href="<?= htmlspecialchars($baseHref) ?>calendar" class="btn btn-outline-primary">📅 Show current week</a>
                <a href="<?= htmlspecialchars($baseHref) ?>subscription" class="btn btn-outline-primary">📜 List current subscriptions</a>
            </div>
        </div>
        <div class="col-sm-8">
            <?php if ($accessToken === null): ?>
                No token found in session. Please login.
            <?php else: ?>
            <form>
                <div class="mb-3">
                    <label for="accessToken" class="form-label">Azure access token</label>
                    <textarea class="form-control" rows="3"><?= htmlspecialchars($accessToken->getToken()) ?></textarea>
                </div>
                <div class="mb-3">
                    <label for="accessToken" class="form-label">Expires</label>
                    <input type="text" class="form-control" value="<?= date('Y-m-d H:i:s', $accessToken->getExpires()) ?>">
                </div>
                <a href="<?= htmlspecialchars($baseHref) ?>auth/refresh" class="btn btn-outline-primary">🔄 Refresh token</a>
                <a href="<?= htmlspecialchars($baseHref) ?>auth/destroy" class="btn btn-outline-warning">🗑️ Destroy session</a>
            </form>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm">
            <h3>Event logbook</h3>
            <pre><?= htmlspecialchars($logbook) ?></pre>
            <a href="<?= htmlspecialchars($baseHref) ?>index/clear" class="btn btn-outline-warning">🗑️ Clear logbook</a>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>