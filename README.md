# Calendar POC

## Run with Docker

1. Download and install [Docker Desktop for Windows](https://hub.docker.com/editions/community/docker-ce-desktop-windows/)
2. Open a `cmd.exe` window in the project folder and start Docker container: `docker-compose up -d`
3. Open a Linux console with `docker-compose exec php bash` (all other tools are available in here) 
4. Execute: `composer install`

## How to test locally with callbacks?

This can be done by building an NGrok tunnel. To setup a tunnel, follow these steps: 

1. Place `ngrok.exe` (available at https://ngrok.com/download) to the bin folder in this project.
2. Create an account at https://dashboard.ngrok.com/signup
3. Execute: `bin\ngrok.exe authtoken [authorization-token]` (You can find the authorization token here: https://dashboard.ngrok.com/auth/your-authtoken)
4. Execute: `bin\ngrok.exe http 81 -region eu -host-header=localhost --subdomain=gripplocal4`

## Register your app with the Microsoft Azure App Portal

1. Register this app to the [Microsoft Azure App Portal](https://go.microsoft.com/fwlink/?linkid=2083908).
2. Get the *"Application (client) ID"* from the **Overview** section in the **App Portal** and place it in `/config/secrets.ini`.
3. Get the *"OAuth 2.0 authorization/token endpoint (v2)"* from the **Overview** section in the **App Portal** By clicking **Endpoints**. Place these in `/config/secrets.ini`.
4. Create a *"Client secret"* on the **Certificates & secrets** section in the **App Portal** and place the value in `/config/secrets.ini`.

Now we need to configure this App:   

1. Go to **Authentication** in the **App Portal** and click **Add a platform** -> **Web** and fill in the **Redirect URIs** with the same value in `/config/framework.ini` section `azure` key `redirect_uri`.
2. Add *"Microsoft Graph -> Delegated permissions -> Calendars.ReadWrite"* to the **API permissions** section in the **App Portal**

Source: https://docs.microsoft.com/en-us/graph/auth-v2-user

## Must read!

https://docs.microsoft.com/en-us/graph/webhooks-lifecycle

