<?php

namespace App\Controller;

use App\Middleware\Session;
use League\OAuth2\Client\Provider\GenericProvider;
use Microsoft\Graph\Core\GraphConstants;
use Microsoft\Graph\Graph;
use Miniframe\Core\AbstractController;
use Miniframe\Core\Config;
use Miniframe\Core\Registry;
use Miniframe\Core\Request;

class AbstractAzureEnabledController extends AbstractController
{
    /**
     * @var GenericProvider
     */
    protected $azureProvider;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var Graph
     */
    protected $graph;

    public function __construct(Request $request, Config $config)
    {
        parent::__construct($request, $config);

        $this->session = Registry::get('session');

        $this->azureProvider = new GenericProvider([
            'clientId'                => $config->get('azure', 'application_id'),
            'clientSecret'            => $config->get('azure', 'client_secret'),
            'redirectUri'             => $config->get('azure', 'redirect_uri'),
            'urlAuthorize'            => $config->get('azure', 'authorize_endpoint'),
            'urlAccessToken'          => $config->get('azure', 'token_endpoint'),
            'urlResourceOwnerDetails' => GraphConstants::REST_ENDPOINT . GraphConstants::API_VERSION . '/me',
        ]);

        $this->graph = new Graph();
        $this->graph->setAccessToken($this->session->get('azure_accesstoken'));
    }
}