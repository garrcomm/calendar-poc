<?php

namespace App\Controller;

use Miniframe\Core\Response;
use Miniframe\Response\PhpResponse;
use Miniframe\Response\RedirectResponse;

class Index extends AbstractAzureEnabledController
{
    public function main(): Response
    {
        $eventLog = $this->config->getPath('azure', 'webhook_logs') . 'logbook.txt';
        $logbook = @file_get_contents($eventLog);

        return new PhpResponse(__DIR__ . '/../../templates/index.html.php', [
            'baseHref' => $this->config->get('framework', 'base_href'),
            'accessToken' => $this->session->get('azure_accesstoken'),
            'logbook' => $logbook,
        ]);
    }

    public function clear(): Response
    {
        $eventLog = $this->config->getPath('azure', 'webhook_logs') . 'logbook.txt';
        file_put_contents($eventLog, '');

        return new RedirectResponse($this->config->get('framework', 'base_href'));
    }
}
