<?php

namespace App\Controller;

use App\TimeZones\TimeZones;
use GuzzleHttp\Exception\ClientException;
use Microsoft\Graph\Model;
use Miniframe\Core\Response;
use Miniframe\Response\InternalServerErrorResponse;
use Miniframe\Response\PhpResponse;
use Miniframe\Response\RedirectResponse;

class Subscription extends AbstractAzureEnabledController
{
    public function main(): Response
    {
        // Fetch subscriptions
        $subscriptions = $this->graph->createRequest('GET', '/subscriptions')
            ->setReturnType(Model\Subscription::class)
            ->execute();

        return new PhpResponse(__DIR__ . '/../../templates/subscriptions.html.php', [
            'subscriptions'  => $subscriptions,
            'homeUrl'        => $this->config->get('framework', 'base_href'),
            'subscribeUrl'   => $this->config->get('framework', 'base_href') . 'subscription/subscribe',
            'unsubscribeUrl' => $this->config->get('framework', 'base_href') . 'subscription/unsubscribe?id=$id',
            'refreshUrl'     => $this->config->get('framework', 'base_href') . 'subscription/refresh?id=$id',
        ]);
    }

    public function subscribe(): Response
    {
        // Specifies the date and time when the webhook subscription expires
        $timezone = TimeZones::getTzFromWindows('W. Europe Standard Time');
        $validUntil = new \DateTimeImmutable('+1 hour', $timezone);

        $subscription = new Model\Subscription();
        $subscription->setChangeType('created,updated,deleted');
        $subscription->setNotificationUrl($this->config->get('azure', 'webhook_uri') . '/event');
        $subscription->setLifecycleNotificationUrl($this->config->get('azure', 'webhook_uri') . '/eventLifecycle');
        $subscription->setResource('me/events');
        $subscription->setExpirationDateTime($validUntil->format(\DateTimeInterface::RFC3339));

        $jsonSubscription = json_encode($subscription, JSON_PRETTY_PRINT);

        try {
            $subscription = $this->graph->createRequest('POST', '/subscriptions')
                ->attachBody($jsonSubscription)
                ->setReturnType(Model\Subscription::class)
                ->execute(); /* @var $subscription Model\Subscription */
            $storageName = $this->config->getPath('azure', 'storage') . '/subscription/' . $subscription->getId();
            file_put_contents($storageName, serialize([
                'subscription' => $subscription,
                'accessToken' => $this->session->get('azure_accesstoken'),
            ]));
        } catch (ClientException $exception) {
            return new InternalServerErrorResponse($exception->getResponse()->getBody(), 0, $exception);
        }

        return new RedirectResponse($this->config->get('framework', 'base_href') . 'subscription');
    }

    public function unsubscribe(): Response
    {
        $id = $this->request->getRequest('id');

        $this->graph->createRequest('DELETE', '/subscriptions/' . rawurlencode($id))
            ->execute();

        return new RedirectResponse($this->config->get('framework', 'base_href') . 'subscription');
    }

    public function refresh(): Response
    {
        $id = $this->request->getRequest('id');

        // Specifies the date and time when the webhook subscription expires
        $timezone = TimeZones::getTzFromWindows('W. Europe Standard Time');
        $validUntil = new \DateTimeImmutable('+2 days', $timezone);

        $subscription = new Model\Subscription();
        $subscription->setExpirationDateTime($validUntil->format(\DateTimeInterface::RFC3339));

        $jsonSubscription = json_encode($subscription, JSON_PRETTY_PRINT);

        $this->graph->createRequest('PATCH', '/subscriptions/' . rawurlencode($id))
            ->attachBody($jsonSubscription)
            ->execute();

        return new RedirectResponse($this->config->get('framework', 'base_href') . 'subscription');
    }
}
