<?php

namespace App\Controller;

use Miniframe\Core\Response;
use League\OAuth2\Client\Token\AccessToken;
use Miniframe\Response\InternalServerErrorResponse;
use Miniframe\Response\RedirectResponse;

class Auth extends AbstractAzureEnabledController
{
    public function destroy(): Response
    {
        $this->session->set('azure_accesstoken', null);

        return new RedirectResponse($this->config->get('framework', 'base_href'));
    }

    public function refresh(): Response
    {
        $token = $this->session->get('azure_accesstoken'); /* @var $token AccessToken */
        $accessToken = $this->azureProvider->getAccessToken('refresh_token', [
            'refresh_token' => $token->getRefreshToken()
        ]);
        $this->session->set('azure_accesstoken', $accessToken);

        return new RedirectResponse($this->config->get('framework', 'base_href'));
    }

    public function login(): Response
    {
        // When something went wrong, we want to show the error
        if ($this->request->getRequest('error')) {
            $error = $this->request->getRequest('error');
            $error .= PHP_EOL . ($this->request->getRequest('error_description') ?? '');
            return new InternalServerErrorResponse($error);
        }

        // If we don't have an authorization code then get one
        if (!$this->request->getRequest('code')) {
            $authorizationUrl = $this->azureProvider->getAuthorizationUrl([
                'scope' => ['Calendars.ReadWrite User.Read offline_access'] // offline_access makes sure we get a refreshToken
            ]);
            $this->session->set('oauth2state', $this->azureProvider->getState());

            return new RedirectResponse($authorizationUrl);
        }

        // Check given state against previously stored one to mitigate CSRF attack
        if (!$this->request->getRequest('state') || ($this->request->getRequest('state') !== $this->session->get('oauth2state'))) {
            $this->session->set('oauth2state', null);

            return new InternalServerErrorResponse('Invalid state');
        }

        // Try to get an access token using the authorization code grant.
        $accessToken = $this->azureProvider->getAccessToken('authorization_code', [
            'code' => $_GET['code']
        ]);

        // Store the access token in the session
        $this->session->set('azure_accesstoken', $accessToken);

        return new RedirectResponse($this->config->get('framework', 'base_href'));
    }
}