<?php

namespace App\Controller;

use App\TimeZones\TimeZones;
use Microsoft\Graph\Model;
use Miniframe\Core\Response;
use Miniframe\Response\PhpResponse;

class Calendar extends AbstractAzureEnabledController
{
    public function main(): Response
    {
        // Snippet from https://docs.microsoft.com/en-us/graph/tutorials/php?tutorial-step=4

        // Get user's timezone
        $timezone = TimeZones::getTzFromWindows('W. Europe Standard Time');

        // Get start and end of week
        $startOfWeek = new \DateTimeImmutable('sunday -1 week', $timezone);
        $endOfWeek = new \DateTimeImmutable('sunday', $timezone);

        // Build request
        $queryParams = array(
            'startDateTime' => $startOfWeek->format(\DateTimeInterface::RFC3339),
            'endDateTime' => $endOfWeek->format(\DateTimeInterface::RFC3339),
            // Only request the properties used by the app
            '$select' => 'subject,organizer,start,end',
            // Sort them by start time
            '$orderby' => 'start/dateTime',
            // Limit results to 25
            '$top' => 25
        );
        $url = '/me/calendarView?' . http_build_query($queryParams);

        // Execute request
        $events = $this->graph->createRequest('GET', $url)
            ->setReturnType(Model\Event::class)
            ->execute();

        return new PhpResponse(__DIR__ . '/../../templates/weekCalendar.html.php', [
            'homeUrl' => $this->config->get('framework', 'base_href'),
            'events'  => $events,
        ]);
    }
}