<?php

namespace App\Controller;

use Microsoft\Graph\Model;
use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\Core\Response;

class Webhook extends AbstractAzureEnabledController
{
    public function __construct(Request $request, Config $config)
    {
        parent::__construct($request, $config);

        // Microsoft wants webhooks to be confirmed operational. We'll do this for all webhooks.
        // See also: https://docs.microsoft.com/en-us/graph/webhooks#notification-endpoint-validation
        if ($this->request->getRequest('validationToken')) {
            $log = '[' . date('Y-m-d H:i:s') . '] Webhook '
                . implode('/', $this->request->getPath())
                .' registered';
            $eventLog = $this->config->getPath('azure', 'webhook_logs') . '/logbook.txt';
            file_put_contents($eventLog, $log . PHP_EOL, FILE_APPEND);

            // Echo the token, well escaped
            $this->logRequest(__FUNCTION__);
            throw new Response(htmlspecialchars($this->request->getRequest('validationToken')));
        }
    }

    public function event(): Response
    {
        $this->logRequest(__FUNCTION__);

        $responseData = json_decode($this->request->getPayload(), true);
        foreach ($responseData['value'] as $cnArray) {
            $changeNotification = new Model\ChangeNotification($cnArray); // Convert event data to a model

            // Fetch the subscription (and user/key) this event is connected to
            $storageName = $this->config->getPath('azure', 'storage') . '/subscription/' . $changeNotification->getSubscriptionId();
            $data = unserialize(file_get_contents($storageName));
            $this->graph->setAccessToken($data['accessToken']);

            $log = '[' . date('Y-m-d H:i:s') . '] ' . $changeNotification->getChangeType()->value();
            $log .= ' ' . $changeNotification->getResourceData()->getODataType();

            if ($changeNotification->getChangeType()->value() != 'deleted') {
                // Request actual object
                try {
                    $event = $this->graph->createRequest('GET', '/' . $changeNotification->getResource())
                        ->setReturnType(Model\Event::class)
                        ->execute();
                    /* @var $event Model\Event */
                    $log .= ' ' . $event->getSubject();
                    $log .= ' (' . $event->getStart()->getDateTime() . ' till ' . $event->getEnd()->getDateTime() . ')';
                    $log .= ' (Change key: ' . $event->getChangeKey() . ')';
                } catch (\GuzzleHttp\Exception\ClientException $exception) {
                    if ($exception->getResponse()->getStatusCode() == 404) {
                        $log .= ' The item cannot be found';
                    } else {
                        $log .= ' ' . get_class($exception) . ' - ' . $exception->getMessage();
                    }
                } catch (\Throwable $throwable) {
                    $log .= ' ' . get_class($throwable) . ' - ' . $throwable->getMessage();
                }
            }
            $log .= ' (Unique ID: ' . $changeNotification->getResourceData()->getId() . ')';

            // Fetch eTag
            $properties = $changeNotification->getResourceData()->getProperties();
            $log .= ' (ETag: ' . $properties['@odata.etag'] . ')';

            $eventLog = $this->config->getPath('azure', 'webhook_logs') . '/logbook.txt';
            file_put_contents($eventLog, $log . PHP_EOL, FILE_APPEND);
        }

        $response = new Response('Accepted');
        $response->setResponseCode(202);
        return $response;
    }

    public function eventLifecycle(): Response
    {
        $this->logRequest(__FUNCTION__);

        return new Response('Accepted', 202);
    }

    /**
     * Logs the Request object and returns the path to the log file (in case we want to append more)
     *
     * @param string $function Name of the function (will be used as suffix for the log file)
     *
     * @return string
     */
    private function logRequest(string $function): string
    {
        // Base filename
        $baseFilename =
            $this->config->getPath('azure', 'webhook_logs')
            . '/' . date('YmdHis_');

        // $filename will always end up unique
        $i = 0;
        while (file_exists($filename = $baseFilename . ($i ? $i . '_' : '') . $function . '.txt')) {
            ++$i;
        }

        file_put_contents($filename, '<?php' . PHP_EOL . '$request = ' . var_export($this->request, true) . ';');

        return $filename;
    }
}