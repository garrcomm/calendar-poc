<?php

namespace App\Middleware;

use Miniframe\Core\AbstractMiddleware;
use Miniframe\Core\Config;
use Miniframe\Core\Request;

class Session extends AbstractMiddleware
{
    public function __construct(Request $request, Config $config)
    {
        parent::__construct($request, $config);

        session_start();
    }

    public function get(string $key)
    {
        return $_SESSION[$key] ?? null;
    }

    public function set(string $key, $value): void
    {
        $_SESSION[$key] = $value;
    }
}