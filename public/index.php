<?php

require_once '../vendor/autoload.php';

exit((new Miniframe\Core\Bootstrap())->run(__DIR__ . '/../'));
